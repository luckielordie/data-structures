#ifndef LIST_H
#define LIST_H

template<class T>
class List
{
private:
	class Node
	{
	public:
		T m_data;
		Node* m_pNext;
		Node(T data)
		{
			m_data = data;
			m_pNext = nullptr;
		}
	};

	Node* GetBackOfListPointer()
	{
		Node* currentNode = m_pHead;
		while (currentNode->m_pNext != nullptr)
		{
			currentNode = currentNode->m_pNext;
		}

		return currentNode;
	}

	Node* GetNodePointerAtIndex(const int index)
	{
		if (index > Count() - 1) return m_pHead;
		int count = 0;
		Node* currentNode = m_pHead;

		while (count < index)
		{
			currentNode = currentNode->m_pNext;
			count++;
		}

		return currentNode;
	}

	int Count()
	{
		int count = 0;
		Node* currentNode = m_pHead;

		while (currentNode != nullptr)
		{
			currentNode = currentNode->m_pNext;
			count++;
		}

		return count;
	}

private:
	Node* m_pHead;
	
public:
	List(T data)
	{
		m_pHead = new Node(data);
	}

	T& Pop_Front()
	{
		Node* newTop = GetNodePointerAtIndex(1);
		T data = m_pHead->m_data;
		m_pHead = newTop;
		return data;
	}

	T& Pop_Back()
	{
		Node* currentBottom = GetBackOfListPointer();
		Node* newBottom = GetNodePointerAtIndex(Count() - 2);
		T data = currentBottom->m_data;
		delete currentBottom;
		currentBottom = nullptr;
		newBottom->m_pNext = nullptr;
		return data;
	}

	void Push_Front(T data)
	{
		Node* newTopNode = new Node(data);
		newTopNode->m_pNext = m_pHead;
		m_pHead = newTopNode;
	}

	void Push_Back(T data)
	{
		Node* bottomNode = GetBackOfListPointer();
		bottomNode->m_pNext = new Node(data);
	}	
};
#endif